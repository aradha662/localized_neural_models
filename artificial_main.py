import numpy as np
import data_generator as dg
import neural_model as nm

def main():
    num_examples = 100
    class_1_name = 'sparse'
    class_0_name = 'dense'
    print(class_1_name, class_0_name)
    class_1 = dg.generate_shapes(num_examples, class_1_name)
    class_0 = dg.generate_shapes(num_examples, class_0_name)

    class_1 = dg.split_into_patches(class_1)
    class_0 = dg.split_into_patches(class_0)
    print("GENERATED TRAINING DATA")

    train_set = np.array(class_1 + class_0)
    train_labels = np.array([1.] * len(class_1) +
                            [0.] * len(class_0))

    num_examples = 10

    v_class_1 = dg.generate_shapes(num_examples, class_1_name)
    v_class_0 = dg.generate_shapes(num_examples, class_0_name)

    v_class_1 = dg.split_into_patches(v_class_1)
    v_class_0 = dg.split_into_patches(v_class_0)
    print("GENERATED VALIDATION DATA")

    val_set = np.array(v_class_1 + v_class_0)
    val_labels = np.array([1.] * len(v_class_1) +
                          [0.] * len(v_class_0))

    nm.train_network(train_set, train_labels,
                     val_set, val_labels)


if __name__ == "__main__":
    main()
