import numpy as np
import cv2
import random
from copy import deepcopy
import random
import math
IMG_SIZE = 128
PATCH_SIZE = 16
NOISE_WIDTH = 128
NOISE_HEIGHT = 128

def generate_shapes(num_examples, shape_name):
    data = []
    center = int((IMG_SIZE - 1) / 2)
    for i in range(num_examples):
        template = np.zeros((IMG_SIZE, IMG_SIZE))
        r, c = template.shape

        focus_region_h = np.random.randint(32, 50)
        focus_region_v = np.random.randint(32, 50)

        if shape_name == 'class_1':
            idx = np.random.randint(0, 80)
            template[idx:10 + idx, idx:10 + idx] = 1.
            data.append(template)
        elif shape_name == 'class_0':
            data.append(template)
        elif shape_name == 'sparse':
            template[center - focus_region_h: center + focus_region_h + 1,
                     center - focus_region_v: center + focus_region_v + 1] = 125 / 255.0
            sparse_size = np.random.randint(13, 15)
            r_idx = center - focus_region_h
            gap_size = np.random.randint(10, 15)
            while r_idx + sparse_size <= center + focus_region_h:
                c_idx = center - focus_region_v
                while c_idx + sparse_size <= center + focus_region_v:
                    template[r_idx:r_idx + sparse_size,
                             c_idx:c_idx + sparse_size] = 255 / 255.
                    c_idx += sparse_size + gap_size
                r_idx += sparse_size + gap_size
            data.append(template)
        elif shape_name == 'dense':
            template[center - focus_region_h: center + focus_region_h + 1,
                     center - focus_region_v: center + focus_region_v + 1] = 125 / 255.0
            dense_size = np.random.randint(3, 7)
            r_idx = center - focus_region_h
            gap_size = np.random.randint(11, 13)
            while r_idx + dense_size <= center + focus_region_h:
                c_idx = center - focus_region_v
                while c_idx + dense_size <= center + focus_region_v:
                    template[r_idx:r_idx + dense_size,
                             c_idx:c_idx + dense_size] = 255 / 255.0
                    c_idx += dense_size + gap_size
                r_idx += dense_size + gap_size
            data.append(template)
        elif shape_name == 'circle':
            radius = random.randint(15, 30)
            new_circle = cv2.circle(template, (center, center),
                                    radius, (255, 255, 255),
                                    thickness=-1)
            data.append(new_circle)
        elif shape_name == 'square':
            side_length = random.randint(30, 50)
            dist = int(side_length / 2)
            initial_point = (center - dist, center - dist)
            end_point = (center + dist, center + dist)
            new_square = cv2.rectangle(template, initial_point,
                                       end_point, (255, 255, 255),
                                       thickness=-1)
            data.append(new_square)
        elif shape_name == 'wood':
            noise = generate_noise(NOISE_WIDTH, NOISE_HEIGHT)
            xy_period = random.randint(9, 12)
            turb_power = 0.2
            turb_size = 32.
            for x in range(c):
                for y in range(r):
                    x_val = (x - NOISE_WIDTH / 2) / NOISE_WIDTH
                    y_val = (y - NOISE_HEIGHT / 2) / NOISE_HEIGHT
                    d_val = math.sqrt(x_val*x_val + y_val*y_val)
                    d_val += turb_power * turbulence(x, y, turb_size, noise) / 256.
                    sine_value = 255. * abs(math.sin(2 * xy_period * d_val * 3.14159))
                    sine_value = min(sine_value, 255.)
                    template[x, y] = int(sine_value) / 255.
            data.append(template)

        elif shape_name == 'wood_aberred':
            noise = generate_noise(NOISE_WIDTH, NOISE_HEIGHT)
            xy_period = random.randint(5, 8)

            turb_power = 0.2
            turb_size = 32.
            for x in range(c):
                for y in range(r):
                    x_val = (x - NOISE_WIDTH / 2) / NOISE_WIDTH
                    y_val = (y - NOISE_HEIGHT / 2) / NOISE_HEIGHT
                    d_val = math.sqrt(x_val*x_val + y_val*y_val)
                    d_val += turb_power * turbulence(x, y, turb_size, noise) / 256.
                    sine_value = 255. * abs(math.sin(2 * xy_period * d_val * 3.14159))
                    sine_value = min(sine_value, 255.)
                    template[x, y] = int(sine_value) / 255.
            data.append(template)

        elif shape_name == 'marble':
            noise = generate_noise(NOISE_WIDTH, NOISE_HEIGHT)
            x_period = random.randint(1, 10)
            y_period = random.randint(1, 10)
            turb_power = 5.
            turb_size = 16.

            for x in range(c):
                for y in range(r):
                    xy_val = x * x_period / NOISE_WIDTH + y * y_period / NOISE_HEIGHT
                    xy_val += turb_power * turbulence(x, y, turb_size, noise) / 256.
                    sine_value = 255 * abs(math.sin(xy_val * 3.14159))
                    sine_value = min(sine_value, 255.)
                    template[x, y] = int(sine_value) / 255.
            data.append(template)
        else:
            break

    return data

def turbulence(x, y, size, noise):
    value = 0.0
    initial_size = size

    while size >= 1:
        value += smooth_noise(x / size, y / size, noise) * size
        size /= 2.0

    return 128.0 * value / initial_size

def generate_noise(r, c):
    return np.random.randint(0, 32768.0, (r, c)) / 32768.0


def smooth_noise(x, y, noise):
    fractX = x - int(x)
    fractY = y - int(y)

    #wrap around
    x1 = (int(x) + NOISE_WIDTH) % NOISE_WIDTH
    y1 = (int(y) + NOISE_HEIGHT) % NOISE_HEIGHT

    #neighbor values
    x2 = (x1 + NOISE_WIDTH - 1) % NOISE_WIDTH
    y2 = (y1 + NOISE_HEIGHT - 1) % NOISE_HEIGHT

    #smooth the noise with bilinear interpolation
    value = 0.0
    value += fractX * fractY * noise[y1, x1]
    value += (1 - fractX) * fractY * noise[y1, x2]
    value += fractX * (1 - fractY) * noise[y2, x1]
    value += (1 - fractX) * (1 - fractY) * noise[y2, x2]

    return value


def split_into_patches(images, color_channels=1):
    split_data = []
    for image in images:
        patches = []
        if color_channels > 1:
            r, c, f = image.shape
        else:
            r, c = image.shape
        i = 0
        while i <= r - PATCH_SIZE:
            j = 0
            while j <= c - PATCH_SIZE:
                new_patch = deepcopy(image[i:i + PATCH_SIZE,
                                           j:j + PATCH_SIZE])
                patches.append(new_patch)
                j += max(2, int(PATCH_SIZE / 3))
            i += max(2, int(PATCH_SIZE / 3))
        split_data.append(patches)
    return split_data


def split_into_tracked_patches(images, color_channels=1):
    split_data = []
    location_data = []
    for image in images:
        patches = []
        loc_data = []
        if color_channels > 1:
            r, c, f = image.shape
        else:
            r, c = image.shape
        i = 0
        while i <= r - PATCH_SIZE:
            j = 0
            while j <= c - PATCH_SIZE:
                new_patch = deepcopy(image[i:i + PATCH_SIZE,
                                           j:j + PATCH_SIZE])
                patches.append(new_patch)
                loc_data.append(((i,j),
                                 (i + PATCH_SIZE, j + PATCH_SIZE)))
                j += 2 # int(PATCH_SIZE / 2)
            i += 2 # int(PATCH_SIZE / 2)
        split_data.append(patches)
        location_data.append(loc_data)
    return split_data, location_data

def extract_patches(images):
    patches = []

    for image in images:
        r, c = image.shape
        i = 0
        while i <= r - PATCH_SIZE:
            j = 0
            while j <= c - PATCH_SIZE:
                new_patch = deepcopy(image[i:i + PATCH_SIZE,
                                           j:j + PATCH_SIZE])
                area = np.sum(new_patch > 0)
                if area >= .5 * PATCH_SIZE * PATCH_SIZE:
                    patches.append(new_patch)
                j += int(PATCH_SIZE / 2)
            i += int(PATCH_SIZE / 2)
    return patches


def extract_all_patches(images):
    patches = []
    locations = []
    step_size = int((PATCH_SIZE - 1) / 2)
    for image in images:
        r, c = image.shape
        for i in range(step_size, r - step_size):
            for j in range(step_size, c - step_size):
                if image[i, j] != 0:
                    new_patch = deepcopy(image[i - step_size:i + step_size + 1,
                                               j - step_size:j + step_size + 1])
                    patches.append(new_patch)
                    locations.append((i, j))
    return patches, locations
