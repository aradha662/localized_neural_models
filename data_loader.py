import h5py
import numpy as np
import PIL
from skimage.transform import rotate
from copy import deepcopy

# All files for training, validation must be stored in one directory
def load_images(dir_name, filenames):
    images = []
    for filename in filenames:
        fname = dir_name + "/" + filename
        image = np.asarray(PIL.Image.open(fname)) / 255.
        IMG_SIZE = 128
        r, c, f = image.shape
        r_idx = 0
        while r_idx <= r - IMG_SIZE:
            c_idx = 0
            while c_idx <= c - IMG_SIZE:
                images.append(deepcopy(image[r_idx:r_idx + IMG_SIZE,
                                             c_idx:c_idx + IMG_SIZE, :]))
                c_idx += IMG_SIZE
            r_idx += IMG_SIZE
    return np.array(images)


def augment_dataset(data):
    new_data = []
    new_labels = []

    for image, label in data:
        new_data.append(image)
        new_labels.append(label)

        new_data.append(rotate(image, 90))
        new_labels.append(label)

        new_data.append(rotate(image, 180))
        new_labels.append(label)

        new_data.append(rotate(image, 270))
        new_labels.append(label)

    return np.array(new_data), np.array(new_labels)


def merge_and_split_data(all_data, all_labels):

    #permuted = np.random.permutation(list(zip(all_data, all_labels)))
    #all_data = np.array([p[0] for p in permuted])
    #all_labels = np.array([p[1] for p in permuted])

    class_idx = 0
    class_data = {}
    n = len(all_labels)

    for idx in range(n):
        label = all_labels[idx]
        example = all_data[idx]

        if label in class_data:
            class_data[label].append((example, all_labels[idx]))
        else:
            class_data[label] = [(example, all_labels[idx])]

    percent_split = .15  # 15 % of all data for validation

    split_idx = -1
    for label in class_data:
        new_idx = int(len(class_data[label]) * percent_split)

        if split_idx != -1:
            split_idx = min(new_idx, split_idx)
        else:
            split_idx = new_idx

    print("Split Idx: ", split_idx)

    train_merge = []
    val_merge = []

    for label in class_data:
        val_merge += class_data[label][0:split_idx]
        #val_merge += class_data[label][0:int(split_idx/2)]
        #val_merge += class_data[label][-int(split_idx/2)::]
        total = len(class_data[label])
        #train_merge += class_data[label][int(split_idx/2):total - int(split_idx/2)]
        train_merge += class_data[label][split_idx:]
    val_merge = np.random.permutation(val_merge)

    train_merge = np.random.permutation(train_merge)

    #train_data, train_labels = augment_dataset(train_merge)
    train_data = np.array([d[0] for d in train_merge], dtype='float32')
    train_labels = np.array([d[1] for d in train_merge], dtype='float32')
    val_data = np.array([d[0] for d in val_merge], dtype='float32')
    val_labels = np.array([d[1] for d in val_merge], dtype='float32')

    print(len(train_data))
    print(len(val_data))
    train = (train_data, train_labels)
    val = (val_data, val_labels)
    return train, val
