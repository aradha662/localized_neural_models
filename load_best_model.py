import numpy as np
import torch
from torch.autograd import Variable
import data_generator as dg
import neural_model as cnn
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

IMAGE_SIZE = 128
PATCH_SIZE = 7

class_1_name = 'wood'
class_0_name = 'marble'
class_1_original = dg.generate_shapes(1, class_1_name)
class_0_original = dg.generate_shapes(1, class_0_name)

class_1, loc_1 = dg.split_into_tracked_patches(class_1_original)
class_0, loc_0 = dg.split_into_tracked_patches(class_0_original)

CONV_SPLIT = np.array(class_1[0]).shape[0]
model = cnn.Net(CONV_SPLIT)
checkpoint = torch.load('model_checkpoint.pth')
model.load_state_dict(checkpoint)
model.cuda()

class_1 = Variable(torch.Tensor(class_1)).cuda()
class_1 = class_1.view(-1, 1, PATCH_SIZE, PATCH_SIZE)
class_0 = Variable(torch.Tensor(class_0)).cuda()
class_0 = class_0.view(-1, 1, PATCH_SIZE, PATCH_SIZE)

modules = list(model.modules())

activations = modules[1](class_1).cpu().data.numpy()
contribs = activations

template = []

for i in range(IMAGE_SIZE):
    row = []
    for j in range(IMAGE_SIZE):
        row.append([0.0])
    template.append(row)

loc_1 = loc_1[0]


for i in range(len(contribs)):
    contrib = contribs[i][0]
    upper, lower = loc_1[i]
    u_r, u_c = upper
    l_r, l_c = lower
    for i in range(u_r, l_r):
        for j in range(u_c, l_c):
            if len(template[i][j]) == 1:
                template[i][j] = [contrib]
            else:
                template[i][j].append(contrib)

for i in range(IMAGE_SIZE):
    for j in range(IMAGE_SIZE):
        template[i][j] = np.mean(np.array(template[i][j]))

plt.imshow(template, 'Greys')
plt.colorbar()
plt.savefig('Class_1_' + class_1_name + '_heatmap.png')
plt.clf()

plt.imshow(class_1_original[0], 'Greys')
plt.colorbar()
plt.savefig('Class_1_' + class_1_name + '_original.png')
plt.clf()


activations = modules[1](class_0).cpu().data.numpy()
contribs = activations

template = []

for i in range(IMAGE_SIZE):
    row = []
    for j in range(IMAGE_SIZE):
        row.append([0.0])
    template.append(row)

loc_0 = loc_0[0]

for i in range(len(contribs)):
    contrib = contribs[i][0]
    upper, lower = loc_0[i]
    u_r, u_c = upper
    l_r, l_c = lower
    for i in range(u_r, l_r):
        for j in range(u_c, l_c):
            if len(template[i][j]) == 1:
                template[i][j] = [contrib]
            else:
                template[i][j].append(contrib)

for i in range(IMAGE_SIZE):
    for j in range(IMAGE_SIZE):
        template[i][j] = np.mean(np.array(template[i][j]))

plt.imshow(template, 'Greys')
plt.colorbar()
plt.savefig('Class_0_' + class_0_name + '_heatmap.png')
plt.clf()

plt.imshow(class_0_original[0], 'Greys')
plt.colorbar()
plt.savefig('Class_0_' + class_0_name + '_original.png')
plt.clf()
