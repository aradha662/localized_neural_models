import numpy as np
import random
import os
import torch
from torch.autograd import Variable
import data_loader as dl
import data_generator as dg
import neural_model as cnn
import matplotlib
import options_parser
matplotlib.use('Agg')
import matplotlib.pyplot as plt

IMAGE_SIZE = 128
PATCH_SIZE = 7
NUM_FILTERS = 64
COLOR_CHANNELS = 3

options = options_parser.setup_data_options()

class_1_data_dir = options.class_1_data_dir
class_1_files = os.listdir(class_1_data_dir)
class_1_name = class_1_data_dir.strip().split('/')[-2]

class_0_data_dir = options.class_0_data_dir
class_0_files = os.listdir(class_0_data_dir)
class_0_name = class_0_data_dir.strip().split('/')[-2]

class_1 = dl.load_images(class_1_data_dir,
                         class_1_files)

class_0 = dl.load_images(class_0_data_dir,
                         class_0_files)

class_1_idx = random.randint(0, len(class_1) - 1)
class_0_idx = random.randint(0, len(class_0) - 1)
class_1_original = [class_1[class_1_idx]]
class_0_original = [class_0[class_0_idx]]

class_1, loc_1 = dg.split_into_tracked_patches(class_1_original,
                                               color_channels=3)
class_0, loc_0 = dg.split_into_tracked_patches(class_0_original,
                                               color_channels=3)

class_1 = np.array(class_1, dtype='float32')
class_0 = np.array(class_0, dtype='float32')
print(class_1.shape)
CONV_SPLIT = np.array(class_1[0]).shape[0]
model = cnn.Net(CONV_SPLIT)
checkpoint = torch.load('model_checkpoint.pth')
model.load_state_dict(checkpoint)
model.cuda()

class_1 = Variable(torch.Tensor(class_1)).cuda()
class_1 = class_1.view(-1, COLOR_CHANNELS, PATCH_SIZE, PATCH_SIZE)
class_0 = Variable(torch.Tensor(class_0)).cuda()
class_0 = class_0.view(-1, COLOR_CHANNELS, PATCH_SIZE, PATCH_SIZE)

modules = list(model.modules())
print(modules[2])
print(modules[9])
print(modules[13])

activations = modules[9](modules[2](class_1).view(-1, PATCH_SIZE * PATCH_SIZE))

activations = activations.view(-1, NUM_FILTERS).cpu().data.numpy()
weights = modules[13].weight.data.cpu().numpy()[0]
weighted_activations = []
for a in activations:
    weighted_activations.append(a * weights)
weighted_activations = np.array(weighted_activations)
print(weighted_activations)
print(weighted_activations[:, 0])

class_1_filter_means = np.mean(weighted_activations, axis=0)

f_size = plt.figaspect(float(2 * NUM_FILTERS) / float(3 * 2))
fig, ax = plt.subplots(nrows=NUM_FILTERS, ncols=2, figsize=f_size)

num_patches, num_filters = weighted_activations.shape
for idx in range(num_filters):

    f_act = weighted_activations[:, idx]
    f_act = f_act / np.max(np.abs(f_act))
    hist, bins = np.histogram(f_act, np.linspace(-1., 1., 50))
    hist = hist / len(f_act)
    ax[idx][0].vlines(bins[:-1], [0], hist)
    ax[idx][0].tick_params(axis='both', which='major', labelsize=4)
    ax[idx][0].tick_params(axis='both', which='minor', labelsize=2)
    print(idx)



activations = modules[9](modules[2](class_0).view(-1, PATCH_SIZE * PATCH_SIZE))

activations = activations.view(-1, NUM_FILTERS).cpu().data.numpy()
weights = modules[13].weight.data.cpu().numpy()[0]
weighted_activations = []
for a in activations:
    weighted_activations.append(a * weights)
weighted_activations = np.array(weighted_activations)
print(weighted_activations)
print(weighted_activations[:, 0])
class_0_filter_means = np.mean(weighted_activations, axis=0)
num_patches, num_filters = weighted_activations.shape

for idx in range(num_filters):
    f_act = weighted_activations[:, idx]
    f_act = f_act / np.max(np.abs(f_act))
    hist, bins = np.histogram(f_act, np.linspace(-1., 1., 50))
    hist = hist / len(f_act)
    ax[idx][1].vlines(bins[:-1], [0], hist)
    ax[idx][1].tick_params(axis='both', which='major', labelsize=4)
    ax[idx][1].tick_params(axis='both', which='minor', labelsize=2)
    print(idx)

fig.subplots_adjust(wspace=.5,
                    hspace=1)

plt.savefig('filter_histograms/all_filters_histograms.pdf')
plt.clf()

filter_list = list(range(NUM_FILTERS))
ind = np.arange(len(filter_list))  # the x locations for the groups
width = 1.0       # the width of the bars

fig, ax = plt.subplots()
p1 = ax.bar(ind, class_0_filter_means, width, color='#d62728', alpha=0.5)
p2 = ax.bar(ind, class_1_filter_means, width, alpha=0.5)
ax.legend((p1, p2), (class_0_name, class_1_name))

plt.savefig('filter_histograms/per_filter_means.png')
plt.clf()

plt.imshow(class_1_original[0])
plt.colorbar()
plt.savefig('filter_histograms/class_1_original.png')
plt.clf()

plt.imshow(class_0_original[0])
plt.colorbar()
plt.savefig('filter_histograms/class_0_original.png')
plt.clf()


activations = modules[1](class_1).cpu().data.numpy()
contribs = activations

template = []

for i in range(IMAGE_SIZE):
    row = []
    for j in range(IMAGE_SIZE):
        row.append([0.0])
    template.append(row)

loc_1 = loc_1[0]


for i in range(len(contribs)):
    contrib = contribs[i][0]
    upper, lower = loc_1[i]
    u_r, u_c = upper
    l_r, l_c = lower
    for i in range(u_r, l_r):
        for j in range(u_c, l_c):
            if len(template[i][j]) == 1:
                template[i][j] = [contrib]
            else:
                template[i][j].append(contrib)

for i in range(IMAGE_SIZE):
    for j in range(IMAGE_SIZE):
        template[i][j] = np.mean(np.array(template[i][j]))

n, bins, patches = plt.hist(list(np.array(template).flatten()),
                            50,
                            normed=1,
                            facecolor='green',
                            alpha=0.75)

plt.savefig('filter_histograms/Class_1_activation_histogram.png')
plt.clf()

plt.imshow(template, 'Greys')
plt.colorbar()
plt.savefig('filter_histograms/Class_1_' + class_1_name + '_heatmap.png')
plt.clf()

activations = modules[1](class_0).cpu().data.numpy()
contribs = activations

template = []

for i in range(IMAGE_SIZE):
    row = []
    for j in range(IMAGE_SIZE):
        row.append([0.0])
    template.append(row)

loc_0 = loc_0[0]

for i in range(len(contribs)):
    contrib = contribs[i][0]
    upper, lower = loc_0[i]
    u_r, u_c = upper
    l_r, l_c = lower
    for i in range(u_r, l_r):
        for j in range(u_c, l_c):
            if len(template[i][j]) == 1:
                template[i][j] = [contrib]
            else:
                template[i][j].append(contrib)

for i in range(IMAGE_SIZE):
    for j in range(IMAGE_SIZE):
        template[i][j] = np.mean(np.array(template[i][j]))

n, bins, patches = plt.hist(list(np.array(template).flatten()),
                            50,
                            normed=1,
                            facecolor='green',
                            alpha=0.75)

plt.savefig('filter_histograms/Class_0_activation_histogram.png')
plt.clf()

plt.imshow(template, 'Greys')
plt.colorbar()
plt.savefig('filter_histograms/Class_0_' + class_0_name + '_heatmap.png')
plt.clf()
