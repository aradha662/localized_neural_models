import numpy as np
import neural_model as nm
import options_parser
import data_loader as dl
import data_generator as dg
import os

def main(options):

    class_1_data_dir = options.class_1_data_dir
    class_1_files = os.listdir(class_1_data_dir)
    class_0_data_dir = options.class_0_data_dir
    class_0_files = os.listdir(class_0_data_dir)

    class_1 = dl.load_images(class_1_data_dir,
                             class_1_files)

    class_0 = dl.load_images(class_0_data_dir,
                             class_0_files)

    #class_1 = class_1[0:300]
    #class_0 = class_0[0:300]

    class_1 = dg.split_into_patches(class_1, color_channels=3)
    class_0 = dg.split_into_patches(class_0, color_channels=3)

    data = np.array(class_1 + class_0)
    labels = np.array([1.] * len(class_1) +
                      [0.] * len(class_0))
    train, val = dl.merge_and_split_data(data, labels)
    train_set, train_labels = train
    val_set, val_labels = val
    print(train_set[0].shape)
    print(val_set[0].shape)
    print(len(train_set), len(train_labels))
    print(len(val_set), len(val_labels))
    print(train_set.shape)
    nm.train_network(train_set, train_labels,
                     val_set, val_labels)


options = options_parser.setup_data_options()
print(options)

if __name__ == "__main__":
    main(options)
