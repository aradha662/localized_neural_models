import torch
import numpy as np
from torch.autograd import Variable, Function
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.nn.init as init
import torch.utils.data as data_utils
import data_generator as dg

PATCH_SIZE = 16
BATCH_SIZE = 16
COLOR_CHANNELS = 1

class SubNet(nn.Module):

    def __init__(self):
        super(SubNet, self).__init__()

        self.convs = nn.Sequential(
            nn.Conv2d(COLOR_CHANNELS, 64, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.ReLU(),
        )

        self.filter_linear = nn.Sequential(
            nn.Linear(PATCH_SIZE * PATCH_SIZE, 1),
            nn.Tanh(),
        )

        self.linear = nn.Sequential(
            nn.Linear(64, 1),
        )

        for m in self.modules():
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
                init.kaiming_normal(m.weight.data)
                m.bias.data.fill_(0.1)

    def forward(self, image):
        conv = self.convs(image)
        per_filter = conv.view(-1, PATCH_SIZE * PATCH_SIZE)
        per_filter = self.filter_linear(per_filter)
        return self.linear(per_filter.view(-1, 64))
        #return self.linear(conv.view(-1, 64 * PATCH_SIZE * PATCH_SIZE))

class Net(nn.Module):

    def __init__(self, CONV_SPLIT):
        super(Net, self).__init__()
        self.CONV_SPLIT = CONV_SPLIT
        self.subnet = SubNet()
        self.sigmoid = nn.Sigmoid()

        for m in self.modules():
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
                init.kaiming_normal(m.weight.data)
                m.bias.data.fill_(0.1)

    # For a gray scale image
    def forward(self, image):
        # For subnet chunk to -1, 1, PATCH_SIZE, PATCH_SIZE
        # Chunk output back to -1, CONV_SPLIT, PATCH_SIZE, PATCH_SIZE

        #image = image.view(-1, COLOR_CHANNELS, PATCH_SIZE, PATCH_SIZE)
        # Use k chunks

        idx = 0
        num_ex = image.size()[0]
        chunk_size = 8
        sigmoid_outs = []

        while idx < num_ex:
            subset = image[idx:min(idx + chunk_size, num_ex), :, :, :]
            subset = subset.view(-1, COLOR_CHANNELS, PATCH_SIZE, PATCH_SIZE)
            subnet_out = self.subnet(subset)
            subnet_out = subnet_out.view(-1, self.CONV_SPLIT)
            sigmoid_outs.append(self.sigmoid(torch.mean(subnet_out, dim=1)))
            idx += chunk_size

        out =  torch.cat(sigmoid_outs, 0)
        return out
        #subnet_out = self.subnet(image)
        #subnet_out = subnet_out.view(-1, self.CONV_SPLIT)
        #return self.sigmoid(torch.mean(subnet_out, dim=1))


def train_network(train_imgs, train_labels,
                  val_imgs, val_labels):

    train_imgs = torch.Tensor(train_imgs)
    CONV_SPLIT = train_imgs.size()[1]
    print(CONV_SPLIT)
    train_labels = torch.Tensor(train_labels)
    val_imgs = torch.Tensor(val_imgs)
    val_labels = torch.Tensor(val_labels)

    train_set = data_utils.TensorDataset(train_imgs, train_labels)
    val_set = data_utils.TensorDataset(val_imgs, val_labels)

    train_loader = data_utils.DataLoader(train_set, batch_size=BATCH_SIZE,
                                         shuffle=True, num_workers=2)

    val_loader = data_utils.DataLoader(val_set, batch_size=BATCH_SIZE,
                                       shuffle=False, num_workers=2)


    net = Net(CONV_SPLIT)
    net.cuda()

    criterion = nn.BCELoss()
    optimizer = optim.Adam(net.parameters(),lr=1e-4)

    patience = 0
    min_val_loss = float("inf")
    epoch = 0

    while patience < 100:
        avg_train_loss = 0.0
        avg_val_loss = 0.0
        epoch += 1
        print("Epoch: ", epoch)
        training_confusion = np.zeros((2, 2))
        validation_confusion = np.zeros((2, 2))
        num_batches = 0
        for i, batch in enumerate(train_loader, 0):
            inputs, targets = batch
            targets = targets.float()
            inputs, targets = Variable(inputs.cuda()), Variable(targets.cuda())

            optimizer.zero_grad()
            outputs = net(inputs)

            loss = criterion(outputs, targets)
            loss.backward()
            optimizer.step()

            logits = outputs.cpu().data.numpy()
            labels = targets.cpu().data.numpy()

            for i in range(len(logits)):
                logit = np.around(logits[i])
                label = labels[i]
                training_confusion[int(label), int(logit)] += 1.

            avg_train_loss += loss.cpu().data.numpy()
            num_batches += 1

        print("Training Loss: ", avg_train_loss / num_batches)
        train_acc = training_confusion[0, 0] + training_confusion[1, 1]
        train_acc /= training_confusion[1, 0] + training_confusion[0, 1] + train_acc
        print("Training Acc: ", train_acc)
        num_batches = 0
        misclassified_class_1 = []
        misclassified_class_0 = []


        for i, batch in enumerate(val_loader, 0):
            inputs, targets = batch
            targets = targets.float()
            inputs, targets = Variable(inputs.cuda()), Variable(targets.cuda())

            outputs = net(inputs)
            loss = criterion(outputs,targets)

            inputs = inputs.cpu().data.numpy()
            logits = outputs.cpu().data.numpy()
            labels = targets.cpu().data.numpy()

            for i in range(len(logits)):
                logit = np.around(logits[i])
                label = labels[i]

                validation_confusion[int(label), int(logit)] += 1.
                if int(label) != int(logit):
                    if int(label) == 1:
                        misclassified_class_1.append(inputs[i])
                    else:
                        misclassified_class_0.append(inputs[i])

                avg_val_loss += loss.cpu().data.numpy()
                num_batches += 1

        if avg_val_loss <= min_val_loss:
            min_val_loss = avg_val_loss
            print("Training Confusion: \n", training_confusion)
            print("Validation Confusion: \n", validation_confusion)
            #if len(misclassified_class_1) > 0:
            #    print("MISCLASSIFIED CLASS 1")
            #    print(misclassified_class_1[0])
            #if len(misclassified_class_0) > 0:
            #    print("MISCLASSIFIED CLASS 0")
            #    print(misclassified_class_0[0])

            torch.save(net.cpu().state_dict(), 'model_checkpoint.pth')
            net.cuda()
        else:
            patience += 1

        print("Validation Loss: ", avg_val_loss / num_batches)
        val_acc = validation_confusion[0, 0] + validation_confusion[1, 1]
        val_acc /= validation_confusion[1, 0] + validation_confusion[0, 1] + val_acc
        print("Validation Acc: ", val_acc)

    return net
