import argparse


def setup_data_options():
    options = argparse.ArgumentParser()

    options.add_argument('-t', action="store", dest='class_1_data_dir',
                    default='/home/cdurham/NeuralModels/localized_neural_models/global_local_model/dtd_data/dtd/images/cracked/')

    options.add_argument('-v', action="store", dest='class_0_data_dir',
                         default='/home/cdurham/NeuralModels/localized_neural_models/global_local_model/dtd_data/dtd/images/perforated/')

    return options.parse_args()
